#
# Install file for Debian-Edu debconf override package
#

INSTALL     = install
INSTALL_DATA= install -m 644
INSTALL_BIN = install -m 755

libdir      = /usr/lib
templatedir = /usr/share/debconf/templates
pkglibdir   = $(libdir)/debian-edu-install
libexecdir  = /usr/lib
pkglibexecdir = $(libexecdir)/debian-edu-install
sbindir     = /usr/sbin
sysconfdir  = /etc

PART_RECIPES = \
        90edumain \
        92edumain+ws \
        96eduwork \
        91edumain+ltsp \
        94edultsp \
        97minimal \
        98edustandalone

PART_ARCHES = \
        recipes \
        recipes-amd64-efi \

WRITE_ARCH_RECIPE = \
	sed -e "/>*>ARCH SPECIFIC<*</ r lib/partman/$$partarch-specific" \
	    -e '/>*>ARCH SPECIFIC<*</ d' \
	lib/partman/common/$$partrecipe > lib/partman/$$partarch/$$partrecipe

# Created using 'file * */*|grep Bourne|cut -d: -f1'
SCRIPTS = $(shell file * */*|egrep 'Bourne|POSIX'|cut -d: -f1)

DEFAULTS = \
	common networked main-server workstation ltsp-server \
	standalone ltsp-chroot

.PHONY: all check install status dist clean profile-demo check-defaults check-scripts install-partman-recipes minimum-diskreq
all: check
check: update-partman-recipes check-scripts # check-defaults

install:
	for profile in $(DEFAULTS) ; do \
	    $(INSTALL_DATA) preseed-values/defaults.$$profile $(DESTDIR)$(pkglibdir)/; \
	done
	$(INSTALL_DATA) version $(DESTDIR)$(pkglibdir)
	$(INSTALL_BIN) debian-edu-testsuite $(DESTDIR)$(pkglibexecdir)

debian-edu-profile.templates: debian/debian-edu-profile-udeb.templates
	(cd debian; po2debconf debian-edu-profile-udeb.templates) > $@

profile-demo: debian-edu-profile.templates
	chmod a+x ./debian-edu-profile
	DEBIAN_FRONTEND=dialog DEBCONF_DEBUG=developer /usr/share/debconf/frontend ./debian-edu-profile
	rm -f debian-edu-profile.templates

# This check need write access to the debconf database.
check-defaults:
	for profile in $(DEFAULTS) ; do \
	    tools/debconf-load-defaults -c preseed-values/defaults.$$profile; \
	done

# Detect typos in /bin/sh scripts
check-scripts:
	for script in $(SCRIPTS) ; do \
		dash -n $$script ; \
	done

update-partman-recipes: lib/partman/common/* lib/partman/recipes*-specific
	for partarch in $(PART_ARCHES) ; do \
            for partrecipe in $(PART_RECIPES) ; do \
               $(WRITE_ARCH_RECIPE) ;\
            done ; \
        done
	LC_ALL=C printf "Last made $@ `date` for:\n$(?F)\n" > $@	# Avoid redundant makes.

install-partman-recipes: update-partman-recipes
	for partarch in $(PART_ARCHES) ; do \
		$(INSTALL_DATA) lib/partman/$$partarch/* $(DESTDIR)/lib/partman/$$partarch ;\
        done

minimum-diskreq:
	@echo "Minimum disk requirements, in MiB:"
	@for profile in lib/partman/recipes*/[0-9]* ; do \
	  awk '$$1 ~ /^[0-9]/ { sum=sum+$$1} END { printf("%d\t%s\n", sum, FILENAME) }' "$$profile" ; \
	done

status:
	for f in debian/po/*.po; do \
		echo -n $$f:; LANG=C msgfmt --statistics -o /dev/null $$f 2>&1 ; \
	done

dist:
	debuild -us -uc

clean:
	$(RM) *~
